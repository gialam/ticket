<?php

namespace Magenest\Ticket\Model;

use Magenest\Ticket\Api\ApiInterface;
use Magento\Catalog\Model\ProductFactory;
use Magento\CatalogInventory\Api\StockStateInterface;
use Magento\Framework\App\ObjectManager;
use Magenest\Ticket\Model\ResourceModel\Ticket;

/**
 * Class Api
 * @package Magenest\Ticket\Model
 */
class Api implements ApiInterface
{
    /** @var TicketFactory */
    protected $_ticketFactory;

    /** @var EventFactory */
    protected $_eventFactory;

    /** @var EventLocationFactory */
    protected $_eventLocationFactory;

    /** @var EventDateFactory */
    protected $_eventDateFactory;

    /** @var EventSessionFactory */
    protected $_eventSessionFactory;

    /** @var  \Magento\Catalog\Model\ProductFactory $productFactory */
    protected $_productFactory;

    /** @var StockStateInterface */
    protected $_stockItem;

    protected $_ticketResource;

    /**
     * Api constructor.
     * @param TicketFactory $ticketFactory
     * @param EventFactory $eventFactory
     * @param EventLocationFactory $eventLocationFactory
     * @param EventDateFactory $eventDateFactory
     * @param EventSessionFactory $eventSessionFactory
     * @param \Magento\Catalog\Model\ProductFactory $productFactory
     * @param StockStateInterface $stockItem
     * @param Ticket $ticketResource
     */
    public function __construct
    (
        TicketFactory $ticketFactory,
        EventFactory $eventFactory,
        EventLocationFactory $eventLocationFactory,
        EventDateFactory $eventDateFactory,
        EventSessionFactory $eventSessionFactory,
        ProductFactory $productFactory,
        StockStateInterface $stockItem,
        Ticket $ticketResource
    )
    {
        $this->_ticketFactory = $ticketFactory;
        $this->_eventFactory = $eventFactory;
        $this->_eventLocationFactory = $eventLocationFactory;
        $this->_eventDateFactory = $eventDateFactory;
        $this->_eventSessionFactory = $eventSessionFactory;
        $this->_productFactory = $productFactory;
        $this->_stockItem = $stockItem;
        $this->_ticketResource = $ticketResource;
    }

    /**
     * @inheritdoc
     */
    public function getTicket($code) {
        $objectManager = ObjectManager::getInstance();

        /** @var \Magenest\Ticket\Api\Data\TicketInterface $ticketDataObject */
        $ticketDataObject = $objectManager->create('\Magenest\Ticket\Model\Data\Ticket');

        $ticketCollection = $this->_ticketFactory->create()->getCollection()->addFieldToFilter('code', $code);
        $eventDateCollection = $this->_eventDateFactory->create()->getCollection();
        $eventLocationCollection = $this->_eventLocationFactory->create()->getCollection();
        $eventSessionCollection = $this->_eventSessionFactory->create()->getCollection();
        if ($ticketCollection->count() == 0) {
            $ticketDataObject->setResponse_code(1);
            $ticketDataObject->setResponse_message('Code Not Found');
            return $ticketDataObject;
        }
        $ticket = $ticketCollection->getFirstItem();
        $info = unserialize($ticket['information']);
        $location = $info['location'];
        $date = $info['date'];
        $session = $info['session'];
        if (isset($location)) {
            $eventLocationCollection->addFieldToFilter('location_id', $location);
        }
        if (isset($date)) {
            $eventDateCollection->addFieldToFilter('date_id', $date);
        }
        if (isset($session)) {
            $eventSessionCollection->addFieldToFilter('session_id', $session);
        }
        $ticketDataObject->setResponse_code(0);
        $ticketDataObject->setResponse_message('Success');
        $ticketDataObject->setTicket_id($ticket['ticket_id']);
        $ticketDataObject->setOrder_id($ticket['order_id']);
        $ticketDataObject->setCustomer_name($ticket['customer_name']);
        $ticketDataObject->setCustomer_email($ticket['customer_email']);
        $ticketDataObject->setTitle($ticket['title']);
        $ticketDataObject->setStatus($ticket['status']);
        $ticketDataObject->setQty($ticket['qty']);
        $ticketDataObject->setNote($ticket['note']);
        if (isset($date) && $eventDateCollection->count() != 0) {
            $eventDate = $eventDateCollection->getFirstItem();
            $ticketDataObject->setDate_start($eventDate['date_start']);
            $ticketDataObject->setDate_end($eventDate['date_end']);
        }
        if (isset($location) && $eventLocationCollection->count() != 0) {
            $eventLocation = $eventLocationCollection->getFirstItem();
            $ticketDataObject->setLocation_title($eventLocation['location_title']);
            $ticketDataObject->setLocation_detail($eventLocation['location_detail']);
        }
        if (isset($session) && $eventSessionCollection->count() != 0) {
            $eventSession = $eventSessionCollection->getFirstItem();
            $ticketDataObject->setStart_time($eventSession['start_time']);
            $ticketDataObject->setEnd_time($eventSession['end_time']);
        }
        return $ticketDataObject;
    }

    /**
     * @inheritdoc
     */
    public function useTicket($code) {
        $objectManager = ObjectManager::getInstance();

        /** @var \Magenest\Ticket\Api\Data\UseCodeResultInterface $useCodeResultDataObject */
        $useCodeResultDataObject = $objectManager->create('\Magenest\Ticket\Model\Data\UseCodeResult');

        $ticketCollection  = $this->_ticketFactory->create()->getCollection()->addFieldToFilter('code', $code);
        /** @var \Magenest\Ticket\Model\Ticket $ticket */
        $ticket = $ticketCollection->getFirstItem();
        if ($ticketCollection->count() == 0) {
            $useCodeResultDataObject->setResponse_code(1);
            $useCodeResultDataObject->setResponse_message('Code Not Found');
        }
        else if ($ticket['status'] != 1) {
            $useCodeResultDataObject->setResponse_code(2);
            $useCodeResultDataObject->setResponse_message('Code Used');
        }
        else {
            $ticket->setData('status', 2);
            $this->_ticketResource->save($ticket);
            $useCodeResultDataObject->setResponse_code(0);
            $useCodeResultDataObject->setResponse_message('Code Unused');
        }
        return $useCodeResultDataObject;
    }

    /**
     * @inheritdoc
     */
    public function getEvents()
    {
        $eventCollection = $this->_eventFactory->create()->getCollection();
        $objectManager = ObjectManager::getInstance();

        /** @var \Magenest\Ticket\Api\Data\EventsInterface $eventsObjectData */
        $eventsObjectData = $objectManager->create('\Magenest\Ticket\Model\Data\Events');

        foreach ($eventCollection as $event) {
            /** @var \Magenest\Ticket\Api\Data\EventInterface $eventObjectData */
            $eventObjectData = $objectManager->create('\Magenest\Ticket\Model\Data\Event');

            $product_id = $event['product_id'];
            $event_id = $event['event_id'];
            /** @var \Magento\Catalog\Model\Product $product */
            $product = $this->_productFactory->create()->getCollection()->addFieldToFilter('entity_id', $product_id)->getFirstItem();
            $available = $this->_stockItem->getStockQty($product_id, $product->getStore()->getWebsiteId());
            $ticketCollection = $this->_ticketFactory->create()->getCollection()->addFieldToFilter('product_id', $product_id);
            $sold = 0;
            $used = 0;
            foreach ($ticketCollection as $ticket) {
                $sold += $ticket['qty'];
                if ($ticket['status'] == '2')
                    $used += $ticket['qty'];
            }
            $eventSessionCollection = $this->_eventSessionFactory->create()->getCollection()->addFieldToFilter('product_id', $product_id);
            $object['event_id'] = $event['event_id'];
            $object['event_name'] = $event['event_name'];
            $object['ticket_sold'] = $sold;
            $object['ticket_used'] = $used;
            $object['available'] = $available;

            $eventObjectData->setEvent_id($event['event_id']);
            $eventObjectData->setEvent_name($event['event_name']);
            $eventObjectData->setTicket_sold($sold);
            $eventObjectData->setTicket_used($used);
            $eventObjectData->setAvailable($available);

            foreach ($eventSessionCollection as $eventSession) {
                /** @var \Magenest\Ticket\Api\Data\SessionInterface $sessionObjectData */
                $sessionObjectData = $objectManager->create('\Magenest\Ticket\Model\Data\Session');

                $session_id = $eventSession['session_id'];
                $date_id = $eventSession['event_date_id'];
                $eventDate = $this->_eventDateFactory->create()->getCollection()->addFieldToFilter('date_id', $date_id)->getFirstItem();
                $location_id = $eventDate['event_location_id'];
                $eventLocation = $this->_eventLocationFactory->create()->getCollection()->addFieldToFilter('location_id', $location_id)->getFirstItem();
                $info = serialize(['location' => $location_id, 'date' => $date_id, 'session' => $session_id]);
                $ticket_sold = $this->_ticketFactory->create()->getCollection()->addFieldToFilter('event_id', $event_id)->addFieldToFilter('information', $info);

                $sold = 0;
                $used = 0;
                foreach ($ticket_sold as $ticket) {
                    $sold += $ticket['qty'];
                    if ($ticket['status'] == '2')
                        $used += $ticket['qty'];
                }

                $sessionObjectData->setSession_id($session_id);
                $sessionObjectData->setStart_time($eventSession['start_time']);
                $sessionObjectData->setEnd_time($eventSession['end_time']);
                $sessionObjectData->setDate_start($eventDate['date_start']);
                $sessionObjectData->setDate_end($eventDate['date_end']);
                $sessionObjectData->setLocation_title($eventLocation['location_title']);
                $sessionObjectData->setLocation_detail($eventLocation['location_detail']);
                $sessionObjectData->setTicket_sold($sold);
                $sessionObjectData->setTicket_used($used);

                $eventObjectData->addSession($sessionObjectData);
            }
            $eventsObjectData->addEvent($eventObjectData);
        }
        return $eventsObjectData;
    }
}