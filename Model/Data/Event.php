<?php
/**
 * Created by PhpStorm.
 * User: doanphihung
 * Date: 03/07/2017
 * Time: 17:53
 */

namespace Magenest\Ticket\Model\Data;

use Magento\Framework\Api\AbstractSimpleObject;
use Magenest\Ticket\Api\Data\EventInterface;

/**
 * Class Event
 * @package Magenest\Ticket\Model\Data
 */
class Event extends AbstractSimpleObject implements EventInterface
{
    /**
     * @return string
     */
    public function getEvent_id()
    {
        return $this->_get(self::event_id);
    }

    /**
     * @return string
     */
    public function getEvent_name()
    {
        return $this->_get(self::event_name);
    }

    /**
     * @return int
     */
    public function getTicket_sold()
    {
        return $this->_get(self::ticket_sold);
    }

    /**
     * @return int
     */
    public function getTicket_used()
    {
        return $this->_get(self::ticket_used);
    }

    /**
     * @return int
     */
    public function getAvailable()
    {
        return $this->_get(self::available);
    }

    /**
     * @return \Magenest\Ticket\Api\Data\SessionInterface[]
     */
    public function getSessions()
    {
        $sessions = $this->_get(self::sessions);
        return (($sessions == null) ? [] : $sessions);
    }

    /**
     * @param $event_id
     * @return $this
     */
    public function setEvent_id($event_id)
    {
        return $this->setData(self::event_id, $event_id);
    }

    /**
     * @param $event_name
     * @return $this
     */
    public function setEvent_name($event_name)
    {
        return $this->setData(self::event_name, $event_name);
    }

    /**
     * @param $ticket_sold
     * @return $this
     */
    public function setTicket_sold($ticket_sold)
    {
        return $this->setData(self::ticket_sold, $ticket_sold);
    }

    /**
     * @param $ticket_used
     * @return $this
     */
    public function setTicket_used($ticket_used)
    {
        return $this->setData(self::ticket_used, $ticket_used);
    }

    /**
     * @param $available
     * @return $this
     */
    public function setAvailable($available)
    {
        return $this->setData(self::available, $available);
    }

    /**
     * @param \Magenest\Ticket\Api\Data\SessionInterface[] $sessions
     * @return $this
     */
    public function setSessions(array $sessions)
    {
        return $this->setData(self::sessions, $sessions);
    }

    /**
     * @param \Magenest\Ticket\Api\Data\SessionInterface $session
     * @return $this
     */
    public function addSession($session)
    {
        /** @var array $sessions */
        $sessions = $this->getSessions();
        $sessions[] = $session;
        return $this->setSessions($sessions);
    }
}