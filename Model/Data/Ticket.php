<?php

/**
 * Created by PhpStorm.
 * User: doanphihung
 * Date: 03/07/2017
 * Time: 15:00
 */

namespace Magenest\Ticket\Model\Data;

use Magento\Framework\Api\AbstractSimpleObject;
use Magenest\Ticket\Api\Data\TicketInterface;

class Ticket extends AbstractSimpleObject implements TicketInterface
{
    /**
     * @return int
     */
    public function getResponse_code()
    {
        return $this->_get(self::response_code);
    }

    /**
     * @return string
     */
    public function getResponse_message()
    {
        return $this->_get(self::response_message);
    }

    /**
     * @return string
     */
    public function getTicket_id()
    {
        return $this->_get(self::ticket_id);
    }

    /**
     * @return string
     */
    public function getOrder_id()
    {
        return $this->_get(self::order_id);
    }

    /**
     * @return string
     */
    public function getCustomer_name()
    {
        return $this->_get(self::customer_name);
    }

    /**
     * @return string
     */
    public function getCustomer_email()
    {
        return $this->_get(self::customer_email);
    }

    /**
     * @return string
     */
    public function getTitle()
    {
        return $this->_get(self::title);
    }

    /**
     * @return int
     */
    public function getStatus()
    {
        return $this->_get(self::status);
    }

    /**
     * @return int
     */
    public function getQty()
    {
        return $this->_get(self::qty);
    }

    /**
     * @return string
     */
    public function getNote()
    {
        return $this->_get(self::note);
    }

    /**
     * @return string
     */
    public function getDate_start()
    {
        return $this->_get(self::date_start);
    }

    /**
     * @return string
     */
    public function getDate_end()
    {
        return $this->_get(self::date_end);
    }

    /**
     * @return string
     */
    public function getLocation_title()
    {
        return $this->_get(self::location_title);
    }

    /**
     * @return string
     */
    public function getLocation_detail()
    {
        return $this->_get(self::location_detail);
    }

    /**
     * @return string
     */
    public function getStart_time()
    {
        return $this->_get(self::start_time);
    }

    /**
     * @return string
     */
    public function getEnd_time()
    {
        return $this->_get(self::end_time);
    }


    /**
     * @param $response_code
     * @return $this
     */
    public function setResponse_code($response_code)
    {
        return $this->setData(self::response_code, $response_code);
    }

    /**
     * @param $response_message
     * @return $this
     */
    public function setResponse_message($response_message)
    {
        return $this->setData(self::response_message, $response_message);
    }

    /**
     * @param $ticket_id
     * @return $this
     */
    public function setTicket_id($ticket_id)
    {
        return $this->setData(self::ticket_id, $ticket_id);
    }

    /**
     * @param $order_id
     * @return $this
     */
    public function setOrder_id($order_id)
    {
        return $this->setData(self::order_id, $order_id);
    }

    /**
     * @param $customer_name
     * @return $this
     */
    public function setCustomer_name($customer_name)
    {
        return $this->setData(self::customer_name, $customer_name);
    }

    /**
     * @param $customer_email
     * @return $this
     */
    public function setCustomer_email($customer_email)
    {
        return $this->setData(self::customer_email, $customer_email);
    }

    /**
     * @param $title
     * @return $this
     */
    public function setTitle($title)
    {
        return $this->setData(self::title, $title);
    }

    /**
     * @param $status
     * @return $this
     */
    public function setStatus($status)
    {
        return $this->setData(self::status, $status);
    }

    /**
     * @param $qty
     * @return $this
     */
    public function setQty($qty)
    {
        return $this->setData(self::qty, $qty);
    }

    /**
     * @param $note
     * @return $this
     */
    public function setNote($note)
    {
        return $this->setData(self::note, $note);
    }

    /**
     * @param $date_start
     * @return $this
     */
    public function setDate_start($date_start)
    {
        return $this->setData(self::date_start, $date_start);
    }

    /**
     * @param $date_end
     * @return $this
     */
    public function setDate_end($date_end)
    {
        return $this->setData(self::date_end, $date_end);
    }

    /**
     * @param $location_title
     * @return $this
     */
    public function setLocation_title($location_title)
    {
        return $this->setData(self::location_title, $location_title);
    }

    /**
     * @param $location_detail
     * @return $this
     */
    public function setLocation_detail($location_detail)
    {
        return $this->setData(self::location_detail, $location_detail);
    }

    /**
     * @param $start_time
     * @return $this
     */
    public function setStart_time($start_time)
    {
        return $this->setData(self::start_time, $start_time);
    }

    /**
     * @param $end_time
     * @return $this
     */
    public function setEnd_time($end_time)
    {
        return $this->setData(self::end_time, $end_time);
    }
}