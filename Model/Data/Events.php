<?php
/**
 * Created by PhpStorm.
 * User: doanphihung
 * Date: 03/07/2017
 * Time: 16:56
 */

namespace Magenest\Ticket\Model\Data;

use Magento\Framework\Api\AbstractSimpleObject;
use Magenest\Ticket\Api\Data\EventsInterface;

class Events extends AbstractSimpleObject implements EventsInterface
{
    /**
     * @return \Magenest\Ticket\Api\Data\EventInterface[]
     */
    public function getEvents()
    {
        $events = $this->_get(self::events);
        return (($events == null) ? [] : $events);
    }

    /**
     * @param \Magenest\Ticket\Api\Data\EventInterface[]
     * @return $this
     */
    public function setEvents(array $events)
    {
        return $this->setData(self::events, $events);
    }

    /**
     * @param \Magenest\Ticket\Api\Data\EventInterface
     * @return $this
     */
    public function addEvent($event)
    {
        /** @var array $events */
        $events = $this->getEvents();
        $events[] = $event;
        return $this->setData(self::events, $events);
    }
}