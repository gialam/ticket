<?php
/**
 * Created by PhpStorm.
 * User: doanphihung
 * Date: 04/07/2017
 * Time: 12:44
 */

namespace Magenest\Ticket\Model\Data;

use Magento\Framework\Api\AbstractSimpleObject;
use Magenest\Ticket\Api\Data\UseCodeResultInterface;

class UseCodeResult extends AbstractSimpleObject implements UseCodeResultInterface
{
    /**
     * @return int
     */
    public function getResponse_code()
    {
        return $this->_get(self::response_code);
    }

    /**
     * @return string
     */
    public function getResponse_message()
    {
        return $this->_get(self::response_message);
    }

    /**
     * @param $response_code
     * @return $this
     */
    public function setResponse_code($response_code)
    {
        return $this->setData(self::response_code, $response_code);
    }

    /**
     * @param $response_message
     * @return $this
     */
    public function setResponse_message($response_message)
    {
        return $this->setData(self::response_message, $response_message);
    }
}