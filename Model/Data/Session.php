<?php
/**
 * Created by PhpStorm.
 * User: doanphihung
 * Date: 03/07/2017
 * Time: 17:53
 */

namespace Magenest\Ticket\Model\Data;

use Magento\Framework\Api\AbstractSimpleObject;
use Magenest\Ticket\Api\Data\SessionInterface;

class Session extends AbstractSimpleObject implements SessionInterface
{
    /**
     * @return string
     */
    public function getSession_id()
    {
        return $this->_get(self::session_id);
    }

    /**
     * @return string
     */
    public function getStart_time()
    {
        return $this->_get(self::start_time);
    }

    /**
     * @return string
     */
    public function getEnd_time()
    {
        return $this->_get(self::end_time);
    }

    /**
     * @return string
     */
    public function getDate_start()
    {
        return $this->_get(self::date_start);
    }

    /**
     * @return string
     */
    public function getDate_end()
    {
        return $this->_get(self::date_end);
    }

    /**
     * @return string
     */
    public function getLocation_title()
    {
        return $this->_get(self::location_title);
    }

    /**
     * @return string
     */
    public function getLocation_detail()
    {
        return $this->_get(self::location_detail);
    }

    /**
     * @return int
     */
    public function getTicket_sold()
    {
        return $this->_get(self::ticket_sold);
    }

    /**
     * @return int
     */
    public function getTicket_used()
    {
        return $this->_get(self::ticket_used);
    }

    /**
     * @param $session_id
     * @return $this
     */
    public function setSession_id($session_id)
    {
        return $this->setData(self::session_id, $session_id);
    }

    /**
     * @param $start_time
     * @return $this
     */
    public function setStart_time($start_time)
    {
        return $this->setData(self::start_time, $start_time);
    }

    /**
     * @param $end_time
     * @return $this
     */
    public function setEnd_time($end_time)
    {
        return $this->setData(self::end_time, $end_time);
    }

    /**
     * @param $date_start
     * @return $this
     */
    public function setDate_start($date_start)
    {
        return $this->setData(self::date_start, $date_start);
    }

    /**
     * @param $date_end
     * @return $this
     */
    public function setDate_end($date_end)
    {
        return $this->setData(self::date_end, $date_end);
    }

    /**
     * @param $location_title
     * @return $this
     */
    public function setLocation_title($location_title)
    {
        return $this->setData(self::location_title, $location_title);
    }

    /**
     * @param $location_detail
     * @return $this
     */
    public function setLocation_detail($location_detail)
    {
        return $this->setData(self::location_detail, $location_detail);
    }

    /**
     * @param $ticket_sold
     * @return $this
     */
    public function setTicket_sold($ticket_sold)
    {
        return $this->setData(self::ticket_sold, $ticket_sold);
    }

    /**
     * @param $ticket_used
     * @return $this
     */
    public function setTicket_used($ticket_used)
    {
        return $this->setData(self::ticket_used, $ticket_used);
    }
}