<?php

namespace Magenest\Ticket\Api;

/**
 * Interface ApiInterface
 * @package Magenest\Ticket\Api
 */
interface ApiInterface
{
    /**
     * @api
     * @param string $code
     * @return \Magenest\Ticket\Api\Data\TicketInterface
     */
    public function getTicket($code);

    /**
     * @api
     * @param string $code
     * @return \Magenest\Ticket\Api\Data\UseCodeResultInterface
     */
    public function useTicket($code);

    /**
     * @api
     * @return \Magenest\Ticket\Api\Data\EventsInterface
     */
    public function getEvents();
}