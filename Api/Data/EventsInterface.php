<?php

/**
 * Created by PhpStorm.
 * User: doanphihung
 * Date: 03/07/2017
 * Time: 14:35
 */

namespace Magenest\Ticket\Api\Data;

interface EventsInterface
{
    const events = 'events';

    /**
     * @return \Magenest\Ticket\Api\Data\EventInterface[]
     */
    public function getEvents();

    /**
     * @param \Magenest\Ticket\Api\Data\EventInterface[]
     * @return $this
     */
    public function setEvents(array $events);

    /**
     * @param \Magenest\Ticket\Api\Data\EventInterface
     * @return $this
     */
    public function addEvent($event);
}