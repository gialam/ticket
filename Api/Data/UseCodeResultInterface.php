<?php
/**
 * Created by PhpStorm.
 * User: doanphihung
 * Date: 04/07/2017
 * Time: 12:43
 */

namespace Magenest\Ticket\Api\Data;

interface UseCodeResultInterface
{
    const response_code = 'response_code';

    const response_message = 'response_ message';

    /**
     * @return int
     */
    public function getResponse_code();

    /**
     * @return string
     */
    public function getResponse_message();

    /**
     * @param $response_code
     * @return $this
     */
    public function setResponse_code($response_code);

    /**
     * @param $response_message
     * @return $this
     */
    public function setResponse_message($response_message);

}