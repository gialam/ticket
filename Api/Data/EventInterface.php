<?php
/**
 * Created by PhpStorm.
 * User: doanphihung
 * Date: 03/07/2017
 * Time: 17:52
 */

namespace Magenest\Ticket\Api\Data;


interface EventInterface
{
    const event_id = 'event_id';

    const event_name = 'event_name';

    const ticket_sold = 'ticket_sold';

    const ticket_used = 'ticket_used';

    const available = 'available';

    const sessions = 'sessions';

    /**
     * @return string
     */
    public function getEvent_id();

    /**
     * @return string
     */
    public function getEvent_name();

    /**
     * @return int
     */
    public function getTicket_sold();

    /**
     * @return int
     */
    public function getTicket_used();

    /**
     * @return int
     */
    public function getAvailable();

    /**
     * @return \Magenest\Ticket\Api\Data\SessionInterface[]
     */
    public function getSessions();

    /**
     * @param $event_id
     * @return $this
     */
    public function setEvent_id($event_id);

    /**
     * @param $event_name
     * @return $this
     */
    public function setEvent_name($event_name);

    /**
     * @param $ticket_sold
     * @return $this
     */
    public function setTicket_sold($ticket_sold);

    /**
     * @param $ticket_used
     * @return $this
     */
    public function setTicket_used($ticket_used);

    /**
     * @param $available
     * @return $this
     */
    public function setAvailable($available);

    /**
     * @param \Magenest\Ticket\Api\Data\SessionInterface[] $sessions
     * @return $this
     */
    public function setSessions(array $sessions);

    /**
     * @param \Magenest\Ticket\Api\Data\SessionInterface $session
     * @return $this
     */
    public function addSession($session);
}