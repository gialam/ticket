<?php
/**
 * Created by PhpStorm.
 * User: doanphihung
 * Date: 03/07/2017
 * Time: 17:54
 */

namespace Magenest\Ticket\Api\Data;

interface SessionInterface
{
    const session_id = 'session_id';
    const start_time = 'start_time';
    const end_time = 'end_time';
    const date_start = 'date_start';
    const date_end = 'date_end';
    const location_title = 'location_title';
    const location_detail = 'location_detail';
    const ticket_sold = 'ticket_sold';
    const ticket_used = 'ticket_used';

    /**
     * @return string
     */
    public function getSession_id();

    /**
     * @return string
     */
    public function getStart_time();

    /**
     * @return string
     */
    public function getEnd_time();

    /**
     * @return string
     */
    public function getDate_start();

    /**
     * @return string
     */
    public function getDate_end();

    /**
     * @return string
     */
    public function getLocation_title();

    /**
     * @return string
     */
    public function getLocation_detail();

    /**
     * @return int
     */
    public function getTicket_sold();

    /**
     * @return int
     */
    public function getTicket_used();

    /**
     * @param $session_id
     * @return $this
     */
    public function setSession_id($session_id);

    /**
     * @param $start_time
     * @return $this
     */
    public function setStart_time($start_time);

    /**
     * @param $end_time
     * @return $this
     */
    public function setEnd_time($end_time);

    /**
     * @param $date_start
     * @return $this
     */
    public function setDate_start($date_start);

    /**
     * @param $date_end
     * @return $this
     */
    public function setDate_end($date_end);

    /**
     * @param $location_title
     * @return $this
     */
    public function setLocation_title($location_title);

    /**
     * @param $location_detail
     * @return $this
     */
    public function setLocation_detail($location_detail);

    /**
     * @param $ticket_sold
     * @return $this
     */
    public function setTicket_sold($ticket_sold);

    /**
     * @param $ticket_used
     * @return $this
     */
    public function setTicket_used($ticket_used);
}