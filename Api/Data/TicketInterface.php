<?php

/**
 * Created by PhpStorm.
 * User: doanphihung
 * Date: 03/07/2017
 * Time: 14:35
 */

namespace Magenest\Ticket\Api\Data;

interface TicketInterface
{
    const response_code = 'response_code';
    const response_message = 'response_message';
    const ticket_id = 'ticket_id';
    const order_id = 'order_id';
    const customer_name = 'customer_name';
    const customer_email = 'customer_email';
    const title = 'title';
    const status = 'status';
    const qty = 'qty';
    const note = 'note';
    const date_start = 'date_start';
    const date_end = 'date_end';
    const location_title = 'location_title';
    const location_detail = 'location_detail';
    const start_time = 'start_time';
    const end_time = 'end_time';

    /**
     * @return int
     */
    public function getResponse_code();

    /**
     * @return string
     */
    public function getResponse_message();

    /**
     * @return string
     */
    public function getTicket_id();

    /**
     * @return string
     */
    public function getOrder_id();

    /**
     * @return string
     */
    public function getCustomer_name();

    /**
     * @return string
     */
    public function getCustomer_email();

    /**
     * @return string
     */
    public function getTitle();

    /**
     * @return int
     */
    public function getStatus();

    /**
     * @return int
     */
    public function getQty();

    /**
     * @return string
     */
    public function getNote();

    /**
     * @return string
     */
    public function getDate_start();

    /**
     * @return string
     */
    public function getDate_end();

    /**
     * @return string
     */
    public function getLocation_title();

    /**
     * @return string
     */
    public function getLocation_detail();

    /**
     * @return string
     */
    public function getStart_time();

    /**
     * @return string
     */
    public function getEnd_time();

    /**
     * @param $response_code
     * @return $this
     */
    public function setResponse_code($response_code);

    /**
     * @param $response_message
     * @return $this
     */
    public function setResponse_message($response_message);

    /**
     * @param $ticket_id
     * @return $this
     */
    public function setTicket_id($ticket_id);

    /**
     * @param $order_id
     * @return $this
     */
    public function setOrder_id($order_id);

    /**
     * @param $customer_name
     * @return $this
     */
    public function setCustomer_name($customer_name);

    /**
     * @param $customer_email
     * @return $this
     */
    public function setCustomer_email($customer_email);

    /**
     * @param $title
     * @return $this
     */
    public function setTitle($title);

    /**
     * @param $status
     * @return $this
     */
    public function setStatus($status);

    /**
     * @param $qty
     * @return $this
     */
    public function setQty($qty);

    /**
     * @param $note
     * @return $this
     */
    public function setNote($note);

    /**
     * @param $date_start
     * @return $this
     */
    public function setDate_start($date_start);

    /**
     * @param $date_end
     * @return $this
     */
    public function setDate_end($date_end);

    /**
     * @param $location_title
     * @return $this
     */
    public function setLocation_title($location_title);

    /**
     * @param $location_detail
     * @return $this
     */
    public function setLocation_detail($location_detail);

    /**
     * @param $start_time
     * @return $this
     */
    public function setStart_time($start_time);

    /**
     * @param $end_time
     * @return $this
     */
    public function setEnd_time($end_time);

}